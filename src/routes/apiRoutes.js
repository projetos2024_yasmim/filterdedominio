const router = require('express').Router()
const alunoController = require('../controller/alunoController')
const teacherController = require('../controller/teacherController')
const JSONPlaceholderController = require('../controller/JSONPlaceholderController')


router.get('/teacherController/', teacherController.getTeacher)
router.post('/aluno/', alunoController.postAluno)
router.put('/aluno/', alunoController.UpdateAluno)
router.delete('/aluno/:id', alunoController.deleteAluno)
router.get('/external/', JSONPlaceholderController.getUsers)
router.get('/external/io',JSONPlaceholderController.getUsersWebsiteID)
router.get('/external/com',JSONPlaceholderController.getUsersWebsiteCOM)
router.get('/external/net',JSONPlaceholderController.getUsersWebsiteNET)
router.get('/external/filter',JSONPlaceholderController.getCountDomain)

module.exports = router